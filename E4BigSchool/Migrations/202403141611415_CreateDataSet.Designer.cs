﻿// <auto-generated />
namespace E4BigSchool.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.4")]
    public sealed partial class CreateDataSet : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateDataSet));
        
        string IMigrationMetadata.Id
        {
            get { return "202403141611415_CreateDataSet"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
